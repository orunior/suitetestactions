﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace NewTestActions.Modules
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The TabIntroduction recording.
    /// </summary>
    [TestModule("2d5ae1fa-a52c-43e8-a43a-c0bd88961711", ModuleType.Recording, 1)]
    public partial class TabIntroduction : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::NewTestActions.Repositories.TabIntroduction repository.
        /// </summary>
        public static global::NewTestActions.Repositories.TabIntroduction repo = global::NewTestActions.Repositories.TabIntroduction.Instance;

        static TabIntroduction instance = new TabIntroduction();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public TabIntroduction()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static TabIntroduction Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'RxMainFrame.PageTabList' at 54;42.", repo.RxMainFrame.PageTabListInfo, new RecordItemIndex(0));
            repo.RxMainFrame.PageTabList.Click("54;42");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'RxMainFrame.RxTabIntroduction.TxtUserName' at 94;13.", repo.RxMainFrame.RxTabIntroduction.TxtUserNameInfo, new RecordItemIndex(1));
            repo.RxMainFrame.RxTabIntroduction.TxtUserName.Click("94;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Hallow' with focus on 'RxMainFrame.RxTabIntroduction.TxtUserName'.", repo.RxMainFrame.RxTabIntroduction.TxtUserNameInfo, new RecordItemIndex(2));
            repo.RxMainFrame.RxTabIntroduction.TxtUserName.PressKeys("Hallow");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'RxMainFrame.RxTabIntroduction.BtnSubmitUserName' at 36;14.", repo.RxMainFrame.RxTabIntroduction.BtnSubmitUserNameInfo, new RecordItemIndex(3));
            repo.RxMainFrame.RxTabIntroduction.BtnSubmitUserName.Click("36;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Success, "User", "", new RecordItemIndex(4));
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Text='Welcome, Hallow!') on item 'RxMainFrame.RxTabIntroduction.LblWelcomeMessage'.", repo.RxMainFrame.RxTabIntroduction.LblWelcomeMessageInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.RxMainFrame.RxTabIntroduction.LblWelcomeMessageInfo, "Text", "Welcome, Hallow!");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'RxMainFrame.RxTabIntroduction.RxLinkBtnReset' at 23;13.", repo.RxMainFrame.RxTabIntroduction.RxLinkBtnResetInfo, new RecordItemIndex(6));
            repo.RxMainFrame.RxTabIntroduction.RxLinkBtnReset.Click("23;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Text='Welcome!') on item 'RxMainFrame.RxTabIntroduction.LblWelcomeMessage'.", repo.RxMainFrame.RxTabIntroduction.LblWelcomeMessageInfo, new RecordItemIndex(7));
            Validate.AttributeEqual(repo.RxMainFrame.RxTabIntroduction.LblWelcomeMessageInfo, "Text", "Welcome!");
            Delay.Milliseconds(100);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
